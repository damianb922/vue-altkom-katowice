import { AuthService, authService } from './AuthService';
import axios from 'axios'
import { AlbumsResponse, ArtistsResponse } from '@/models/Album';

axios.interceptors.request.use((config) => {
    config.headers['Authorization'] = `Bearer ${authService.getToken()}`;
    return config
})
axios.interceptors.response.use(success => success, error => {
    if (error.response.status === 401) {
        authService.authorize()
    }
    return Promise.reject(error)
})

export class MusicSearchService {

    constructor(
        private search_url = '',
        private auth: AuthService
    ) { }

    searchAlbums(query = 'batman') {
        return axios.get<AlbumsResponse>(this.search_url, {
            params: {
                type: 'album',
                q: query
            }
        })
            .then(resp => resp.data.albums.items)
            .catch(error => Promise.reject(error.response.data.error))
    }

    searchArtists(query = 'batman') {
        return axios.get<ArtistsResponse>(this.search_url, {
            params: {
                type: 'artist',
                q: query
            }
        })
            .then(resp => resp.data.artists.items)
            .catch(error => Promise.reject(error.response.data.error))
    }

}

export const musicSearch = new MusicSearchService(
    'https://api.spotify.com/v1/search', authService
)

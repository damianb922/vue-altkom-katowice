import { Track } from './Track';

// class Playlist{}

export interface Playlist {
    id: number
    name: string;
    favorite: boolean,
    /**
     * HEX Color
     */
    color: string
    // tracks: Array<Track>
    tracks?: Track[]
}

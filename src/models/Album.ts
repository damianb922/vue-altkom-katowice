export interface Entity {
    id: string
    name: string
}

export interface Album extends Entity {
    type: 'album'
    images: AlbumImage[]
    artists?: Artist[]
}

export interface Artist extends Entity { }

export interface AlbumImage {
    url: string
    width?: number
    height?: number
}

export interface AlbumsResponse {
    albums: PagingObject<Album>
}

export interface ArtistsResponse {
    artists: PagingObject<Artist>
}

export type SpotifyResponse = AlbumsResponse | ArtistsResponse;

export interface PagingObject<T> {
    items: T[]
    limit?: number
    offset?: number
}
import { Module } from 'vuex';
import { AppState } from '../store';

export interface Counterstate {
  counter: number;
}

export const counter: Module<Counterstate, AppState> = {
  namespaced:true,
  state: {
    counter: 0
  },
  mutations: {
    INCREMENT(state, payload = 1) {
      state.counter += payload;
    },
    DECREMENT(state, payload = 1) {
      state.counter -= payload;
    },
  },
  actions: {
    increment({ dispatch, commit, state, getters }) {
      commit('INCREMENT');
    },
    decrement({ commit, state: { counter }, dispatch }) {
      if (counter > 0) {
        commit('DECREMENT');
      }
    }
  },
  getters: {
    counter(state) {
      return state.counter
    }
  }
};

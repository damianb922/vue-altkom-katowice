import { Module, VuexModule, Mutation, Action } from 'vuex-module-decorators'
import { Playlist } from '@/models/Playlist';

@Module({})
export default class PlaylistsModule extends VuexModule {

    items: Playlist[] = [{
        id: 123,
        name: "Vue Hits!",
        favorite: false,
        color: "#ff00ff"
    },
    {
        id: 234,
        name: "Vue TOP20",
        favorite: true,
        color: "#ffff00"
    },
    {
        id: 345,
        name: "Best of Vue",
        favorite: false,
        color: "#00ffff"
    }]
    selectedId: Playlist['id'] | null = null
    search_query = ''

    @Mutation
    SET_QUERY(query: string) {
        this.search_query = query
    }

    @Mutation
    UPDATE_PLAYLIST(playlist: Playlist) {
        this.items = this.items.map(p => p.id == playlist.id ? playlist : p)
    }

    @Mutation
    SET_SELECTED(selectedId: Playlist['id']) {
        this.selectedId = selectedId
    }

    // @Action({})
    @Action({ commit: 'SET_SELECTED' })
    selectPlaylist(selected: Playlist) {
        return (!this.selectedId || this.selectedId !== selected.id) ?
            selected.id : null;
    }

    @Action({})
    savePlaylist(draft: Playlist) {
        this.context.commit('UPDATE_PLAYLIST', draft)
    }

    @Action({ commit: 'SET_QUERY' })
    filterPlaylists(query: string) {
        return query
    }

    get playlists() {
        return this.items
    }

    get selected() {
        return this.items.find(p => p.id == this.selectedId)
    }

    get filtered() {
        return this.playlists.filter(p =>
            p.name.toLowerCase().includes(this.search_query.toLowerCase())
        );
    }

    get query() { return this.search_query }

    // @Action({})
    // loadPlaylists() {
    //     const { commit } = this.context;
    //     // parallel
    //     Promise.all([fetch('A'), fetch('B')]).then(([A, B]) => {
    //         commit('A_LOADED', A)
    //         commit('B_LOADED', B)
    //     })
    //     // sequence
    //     fetch('')
    //         .then(data => fetch('more_data'))
    //         .then(data => commit('DATA_LOADED', data))
    //         .catch(error => commit('SET_ERROR', error))
    // }
}
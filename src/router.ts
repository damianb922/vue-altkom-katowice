import Vue from 'vue';
import Router from 'vue-router';
import Home from './views/Home.vue';
import PlaylistsView from './views/Playlists.vue';
import MusicSearchView from './views/MusicSearch.vue';
import Component from 'vue-class-component';

Vue.use(Router);
// Vue.component('router-link',Routerlink)

export default new Router({
  // mode: 'hash',
  mode: 'history',
  linkActiveClass:'active',
  base: process.env.BASE_URL,
  routes: [
    {
      path: '/',
      name: 'home',
      redirect: '/music'
    },
    {
      path: '/playlists',
      name: 'playlists',
      component: PlaylistsView,
      // beforeEnter( to, from, next){ next() }
    },
    {
      path: '/music',
      name: 'musicsearch',
      component: MusicSearchView,
    },
    {
      path: '/about',
      name: 'about',
      // route level code-splitting
      // this generates a separate chunk (about.[hash].js) for this route
      // which is lazy-loaded when the route is visited.
      component: () => import(/* webpackChunkName: "about" */ './views/About.vue'),
    },
    {
      path: '**',
      redirect: '/'
    }
  ],
});
